import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class App {
    public static void main(String[] args) throws Exception {
        // Initialisation
        ServerSocket s = new ServerSocket(114);

        // Quand un client se connecte, on l'accepte automatiquement 
        Socket client = s.accept();
        System.out.println("Un client s'est connecté");

        // Permet de récupérer un string à partir d'un flux
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
        String m = "";
            // Tant que le client n'envoies pas de message null, on le print dans la console
            while ((m = br.readLine()) != null){
                System.out.println(m);
            }
        // Quand le client envoies un message null ça ferme la conneection
        System.out.println("Le client s'est deconnecté ...");
        s.close();
    
    }
}
