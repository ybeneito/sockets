import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Consoleclient {
    /**
     * Permet de discuter avec le socket serv depuis la console
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // Initialisation de la connextion
        Socket s = new Socket("localhost", 114);

        // Permet d'envoyer un string dans un stream
        BufferedWriter bw = new BufferedWriter(new PrintWriter(s.getOutputStream()));
        // Permet de récupérer des entrée users sepuis la console 
        Scanner scan = new Scanner(System.in);

        System.out.println("Vous êtes connecté");
        String m = "";

        /**
         * Cette boucle infinie est là pour indiquer à notre programme d'attendre plusieurs message
         * Si l'utilisateur envoies la lettre "Q" celui ci quitte la boucle et clos la conversation 
         */
        while(true) {
            m = scan.nextLine();
            if(m.equals("Q")) break;
            bw.write(m);
            bw.newLine();
            bw.flush();
        }

        // Fermeture de la connection
        System.out.println("Vous vous déconnectez");
        scan.close();
        s.close();
    }
}
